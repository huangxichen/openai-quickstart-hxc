优化项

1，提供rest api接口提供翻译服务

2，接口提供目标语言传参配置target_language，即支持翻译成多种语言

3，优化翻译后pdf文件的布局

目前提供tests目录下的文件翻译服务

启动命令：

```bash
python api.py --model_type=OpenAIModel --openai_model=gpt-3.5-turbo --openai_api_key=sk-zroA*******4VxY
```



接口调用示例：

```url
http://127.0.0.1:8080/api/translate
```



传参示例：

```json
{
    "filename": "test.pdf",
    "target_language": "中文",
    "target_format":"markdown"
}
```

