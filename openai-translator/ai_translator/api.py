import sys
import os
import json
import io
import pdfplumber
from flask import Flask, request, send_from_directory
from flask_cors import CORS

sys.path.append(os.path.dirname(os.path.abspath(__file__)))

from utils import ArgumentParser, ConfigLoader, LOG
from model import GLMModel, OpenAIModel
from translator import PDFTranslator

app = Flask(__name__)
CORS(app)

argument_parser = ArgumentParser()
args = argument_parser.parse_arguments()
config_loader = ConfigLoader(args.config)

config = config_loader.load_config()

model_name = args.openai_model if args.openai_model else config['OpenAIModel']['model']
api_key = args.openai_api_key if args.openai_api_key else config['OpenAIModel']['api_key']
model = OpenAIModel(model=model_name, api_key=api_key)

pdf_file_path = args.book if args.book else config['common']['book']
#file_format = args.file_format if args.file_format else config['common']['file_format']

# 实例化 PDFTranslator 类，并调用 translate_pdf() 方法
translator = PDFTranslator(model)

@app.route('/api/translate', methods=['POST'])
def translate():
    data = request.get_data()
    # 解析 data
    json_data = json.loads(data.decode('utf-8'))
    if json_data.get('filename') == '':
        return '未选择文件！[test.pdf, The_Old_Man_of_the_Sea.pdf]', 400
    pdf_file_path = '../tests/' + json_data.get('filename')
    target_language = json_data.get('target_language') if json_data.get('target_language') else '中文'
    target_format = json_data.get('target_format') if json_data.get('target_format') else config['common']['file_format']
    if target_format.lower() == "pdf":
        target_format_sample = 'pdf'
    elif target_format.lower() == "markdown":
        target_format_sample = 'md'
    else:
        raise ValueError(f"Unsupported file format: {target_format}")
    translated_file_name = json_data.get('filename').replace('.pdf', f'_translated.' + target_format_sample)
    output_file_path = './translated_file/' + translated_file_name
    translator.translate_pdf(pdf_file_path, target_format, target_language, output_file_path)
    return send_from_directory('./translated_file', translated_file_name, as_attachment=True)

if __name__ == '__main__':
    app.run(port=8080, debug=True)